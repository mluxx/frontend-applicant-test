const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = function () {
    return {
        mode: 'production',
        entry: './src/main.ts',
        output: {
           path: __dirname + '/dist',
           filename: 'app.js'
        },
        plugins: [
            new CopyWebpackPlugin([
                { from: 'src/assets', to: 'assets'}
            ]), 
            new HtmlWebpackPlugin({
                template: __dirname + '/index.html',
                output: __dirname + '/dist',
                inject: 'head'
            })
        ]
    };
}