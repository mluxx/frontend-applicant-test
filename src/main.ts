import * as angular from 'angular';

// declare a module
let myAppModule = angular.module('myApp', []);

myAppModule.filter('filterCountries',[])
.filter('filterCountries', function() {
  return function(input, search) {
    if (!input) return input;
    if (!search) return input;
    var expected = ('' + search);
    var result = {};
    angular.forEach(input, function(value, key) {
      var actual = ('' + key);
      if (actual.indexOf(expected) !== -1) {
        result[key] = value;
      }
    });
    return result;
  }
});

myAppModule.controller('appController', ['$http','$scope',
    function($http, $scope) {
        $scope.selectedCountry = null;
        $http({
          method: 'GET',
          url: 'data.json'
        }).then(function successCallback(response) {
          $scope.countries = response.data;
          // var count = 0;
          // $scope.devicesCount = $scope.countries.forEach(element => {
          //   count += element.devices;
          // });
          let count = 0;
          for (let country of Object.values($scope.countries)) {  
            console.log(country.Devices);
            let devices = country.Devices;
            count += country.Devices;
          }
          $scope.devicesCount = count;

          let countChannels = 0;
          for (let country of Object.values($scope.countries)) {  
            console.log(country.Channels);
            let channels = country.Channels;
            countChannels += country.Channels;
          }
          $scope.channelsCount = countChannels;
          }, function errorCallback(response) {
        });
        
    }
]);

// angular.module('FilterInControllerModule', []).
// controller('FilterController', ['filterFilter', '$scope', function FilterController(filterFilter, $scope) {
//   this.array = $scope.countries;
//   this.filteredArray = filterFilter(this.array, 'a');
// }]);